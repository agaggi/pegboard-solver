# Pegboard Solver

Pegboards of various dimensions, **4x4** through **8x8**, are solved by search algorithms.
These algorithms are:

1. Breadth First Search
2. Depth First Search
3. Greedy Best First Search
4. A* Search

## Dependencies

- [Openjdk 11](https://jdk.java.net/java-se-ri/11); however, newer versions may work
- [Gradle](https://gradle.org/)

## Execution

The project will need to be built using `gradle`:

```
gradle jar
```

Under `build/lib`, the `pegboard-solver-3.0.jar` file will be created. It can be run as follows:

```
java -jar pegboard-solver-3.0.jar {search-algorithm} {dimension}
```

### Arguments

For the **search algorithm** argument, the following options are valid:

| Argument | Algorithm                |
|----------|--------------------------|
| breadth  | Breadth First Search     |
| depth    | Depth First Search       |
| greedy   | Greedy Best First Search |
| a-star   | A* Search                |

Regarding **dimension**, integer values within the interval [4, 8] are valid.

## Program Behavior

Some things to keep in mind: 

- Times for pegboards of size 5+ vary greatly and can take multiple days to complete 
- The effectiveness of some algorithms cannot be shown by running 4x4 boards
- Be mindful of memory usage when running larger boards (i.e. 7x7 and 8x8)

### Breadth First Search

Of all the search algorithms, Breadth First takes the longest as it works down the fringe 
generation by generation. This makes finding solutions on boards greater than 5x5 infeasible,
with even 5x5 taking on average 1.5 hours. 

When running the algorithm on 4x4 pegboards, solutions are consistently found after **1869** 
unique states and fail at either **1686** or **2114** states if no solution.

Example output:

```
1 1 1 1
0 1 1 1
1 1 1 1
1 1 1 1

0 0 0 0
0 0 0 0
0 0 0 1
0 0 0 0

-- Goal State Found --
Unique states explored: 1869
Search time: 0.035s
```

```
1 1 1 1 1
1 1 1 1 0
1 1 1 1 1
1 1 1 1 1
1 1 1 1 1

-- No Goal State Found --
Unique states explored: 706390
Search time: 5032.219s
```

### Depth First Search

Depth First search works its way down the fringe, searching from left to right. If solutions
exist, they can be found much faster; however, the benefits are lost if there is none. This was 
the case when running this search algorithm against pegboards of varying dimensions. For the 4x4
board, solutions were found under **200** states if one existed.

Example solutions:

```
1 1 1 1
1 1 1 1
1 1 1 0
1 1 1 1

0 0 0 0
1 0 0 0
0 0 0 0
0 0 0 0

-- Goal State Found --
Unique states explored: 44
Search time: 0.007s
```

```
1 1 1 1 1
1 1 1 1 1
1 1 1 0 1
1 1 1 1 1
1 1 1 1 1

0 0 0 0 0
0 0 0 0 0
0 0 0 0 1
0 0 0 0 0
0 0 0 0 0

-- Goal State Found --
Unique states explored: 44491
Search time: 10.533s
```

```
1 1 1 1 1 0
1 1 1 1 1 1
1 1 1 1 1 1
1 1 1 1 1 1
1 1 1 1 1 1
1 1 1 1 1 1

0 0 0 0 0 0
0 0 0 0 0 0
0 0 0 0 0 0
0 0 0 0 0 1
0 0 0 0 0 0
0 0 0 0 0 0

-- Goal State Found --
Unique states explored: 6864
Search time: 0.20500000000000002s
```

### Greedy Best First Search

Greedy Best First search is Depth First search with a heuristic. Using a heuristic allows 
for pegboard states that appear more "promising" to be selected over others, resulting in 
a solution being found quicker in some cases. **Heuristics do not guarantee an optimal path 
is being taken.**

For this program, [Manhattan Distance](https://en.wikipedia.org/wiki/Taxicab_geometry) is 
used as a heuristic and was selected as it is more likely for a solution to be obtained when 
pegs are more towards the center.

The algorithm runs similarly to Depth First search but finds solutions quicker on average.

Example solutions:

```
1 1 0 1 
1 1 1 1 
1 1 1 1 
1 1 1 1 

0 0 0 0 
0 0 0 0 
0 0 0 0 
0 1 0 0 

-- Goal State Found --
Unique states explored: 49
Search time: 0.011s
```

```
1 1 0 1 1 
1 1 1 1 1 
1 1 1 1 1 
1 1 1 1 1 
1 1 1 1 1 

0 0 0 0 0 
0 0 0 0 0 
0 0 0 0 0 
0 0 0 0 0 
0 0 1 0 0 

-- Goal State Found --
Unique states explored: 5014
Search time: 0.186s
```

```
1 1 1 1 1 1 
1 1 1 1 1 1 
1 1 1 1 1 0 
1 1 1 1 1 1 
1 1 1 1 1 1 
1 1 1 1 1 1 

0 0 0 0 0 0 
0 0 0 0 0 0 
0 0 0 0 0 1 
0 0 0 0 0 0 
0 0 0 0 0 0 
0 0 0 0 0 0 

-- Goal State Found --
Unique states explored: 114525
Search time: 52.614000000000004s
```

### A* Search

A* search extends Greedy Best First search by applying another heuristic such as **path cost** 
alongside the first heuristic to help make more informed decisions and find the optimal path. 

In the case of pegboards, **path cost** is implemented alongside Manhattan Distance to help 
increase finding a solution in an efficient manner. When running with 4x4 boards, solutions are
found mostly under 70 states.

Example solutions:

```
1 1 0 1
1 1 1 1
1 1 1 1
1 1 1 1

0 0 0 0
0 0 0 0
0 0 0 0
0 1 0 0

-- Goal State Found --
Unique states explored: 49
Search time: 0.01s
```

```
1 1 1 1 1
1 1 1 1 1
1 1 1 1 0
1 1 1 1 1
1 1 1 1 1

0 0 0 0 0
0 0 0 0 0
1 0 0 0 0
0 0 0 0 0
0 0 0 0 0

-- Goal State Found --
Unique states explored: 270
Search time: 0.024s
```

```
1 1 1 1 1 
1 1 1 1 1 
1 1 1 1 1 
0 1 1 1 1 
1 1 1 1 1 

-- No Goal State Found --
Unique states explored: 706390
Search time: 3702.0660000000003s
```