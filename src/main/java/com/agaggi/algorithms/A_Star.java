package com.agaggi.algorithms;

import java.util.*;
import java.lang.Math;

import com.agaggi.Algorithm;
import com.agaggi.Pegboard;

public class A_Star extends Algorithm {

    private int statesExplored = 0;
    private long startTime;
    private final int size;

    private final ArrayDeque<Pegboard> queue;
    private final ArrayList<String> visitedStates;

    /**
     * Class implements the <b>A*</b> search algorithm to solve a pegboard puzzle.
     * @param size The dimension of the pegboard (i.e. 4 -> 4x4)
     */
    public A_Star(int size) {

        this.size = size;
        this.queue = new ArrayDeque<>();
        this.visitedStates = new ArrayList<>();
    }


    @Override
    public void run() {

        Pegboard initialPegboard = new Pegboard(this.size);

        initialPegboard.initialize();
        initialPegboard.print();
        this.generateSuccessors(initialPegboard);
    }


    @Override
    protected void generateSuccessors(Pegboard pegboard) {

        this.startTime = System.currentTimeMillis();
        this.queue.add(pegboard);

        while (!this.queue.isEmpty()) {

            // The pegboard state with the least sum of Manhattan distance and path cost will be first
            Pegboard state = this.queue.removeFirst();
            ArrayList<Pegboard> successors = super.getNextMoves(state);

            for (Pegboard successor : successors) {

                // If not visited a state, add it to the queue and check if it's a goal state
                if (!this.visitedStates.contains(successor.toString())) {

                    this.visitedStates.add(successor.toString());
                    this.queue.add(successor);
                    this.statesExplored++;

                    if (super.isCompleted(successor.getBoard())) {

                        successor.print();
                        super.printResults(this.startTime, this.statesExplored, true);
                    }
                }
            }

            // Using the heuristic to get the "most promising" state
            this.manhattanDistance();
        }

        // No goal state was found if we reach here
        super.printResults(this.startTime, this.statesExplored, false);
    }


    /**
     * An implementation of Manhattan Distance serving as a heuristic function.<br><br>
     *
     * Manhattan Distance is determined by the sum of all pegs' distance to the center of the board.
     * Typically, it is best to work inwards on pegboards so there are no stuck outside pieces.<br><br>
     *
     * This function sorts successors by their total Manhattan Distance <b>AND</b> path cost.
     */
    private void manhattanDistance() {

        // Represents the "center" coordinate of a pegboard. Ex: 5x5 would have a center coordinate of (2, 2)
        final int center = this.size / 2;

        // Will hold pegboard states along with their fitness value
        HashMap<Pegboard, Integer> heuristicValues = new HashMap<>();

        for (Pegboard pegboard : this.queue) {

            int distance = 0;

            for (int i = 0; i < this.size; i++) {

                for (int j = 0; j < this.size; j++) {

                    // If we're on a peg, get its distance from the center and add it to the total
                    if (pegboard.getBoard()[i][j] == 1) {

                        distance += Math.abs(i - center) + Math.abs(j - center);
                    }
                }
            }

            // Add path cost to Manhattan distance to determine optimal path
            heuristicValues.put(pegboard, distance + this.getPathCost(pegboard.getBoard()));
        }

        // Sort by Manhattan distance (lower is better)
        ArrayList<Map.Entry<Pegboard, Integer>> entries = new ArrayList<>(heuristicValues.entrySet());
        Collections.sort(entries, Map.Entry.comparingByValue());

        this.queue.clear();

        for (Map.Entry<Pegboard, Integer> entry : entries) {

            this.queue.add(entry.getKey());
        }
    }


    /**
     * Determines the path cost of a board state. The number of pegs missing represents the cost.
     *
     * @param board The state to be evaluated
     * @return The path cost of the state
     */
    private int getPathCost(int[][] board) {

        final int initialPegs = (int) (Math.pow(this.size, 2) - 1);
        int currentPegs = 0;

        for (int[] row : board) {

            for (int peg : row) {

                if (peg == 1) {

                    currentPegs++;
                }
            }
        }

        return initialPegs - currentPegs;
    }
}
