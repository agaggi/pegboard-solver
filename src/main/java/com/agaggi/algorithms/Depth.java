package com.agaggi.algorithms;

import java.util.ArrayList;
import java.util.ArrayDeque;

import com.agaggi.Algorithm;
import com.agaggi.Pegboard;

public class Depth extends Algorithm {

    private int statesExplored = 0;
    private long startTime;
    private int size;

    private final ArrayDeque<Pegboard> queue;
    private final ArrayList<String> visitedStates;

    /**
     * Class implements the <b>Depth First</b> search algorithm to solve a pegboard puzzle.
     * @param size The dimension of the pegboard (i.e. 4 -> 4x4)
     */
    public Depth(int size) {

        this.size = size;
        this.queue = new ArrayDeque<>();
        this.visitedStates = new ArrayList<>();
    }


    @Override
    public void run() {

        Pegboard initialPegboard = new Pegboard(this.size);

        initialPegboard.initialize();
        initialPegboard.print();
        this.generateSuccessors(initialPegboard);
    }


    @Override
    protected void generateSuccessors(Pegboard pegboard) {

        this.startTime = System.currentTimeMillis();
        this.queue.add(pegboard);

        while (!this.queue.isEmpty()) {

            // Continues making moves for a pegboard state until a goal/fail state is reached
            Pegboard state = this.queue.removeLast();
            ArrayList<Pegboard> successors = super.getNextMoves(state);

            for (Pegboard successor : successors) {

                // If not visited a state, add it to the queue and check if it's a goal state
                if (!this.visitedStates.contains(successor.toString())) {

                    this.visitedStates.add(successor.toString());
                    this.queue.add(successor);
                    this.statesExplored++;

                    if (super.isCompleted(successor.getBoard())) {

                        successor.print();
                        super.printResults(this.startTime, this.statesExplored, true);
                    }
                }
            }
        }

        // No goal state was found if we reach here
        super.printResults(this.startTime, this.statesExplored, false);
    }
}
