package com.agaggi;

import java.util.ArrayList;

public abstract class Algorithm {

    /**
     * Serves as the "initializer" function of the algorithm; takes care of pegboard setup etc.
     */
    public abstract void run();


    /**
     * Generates successors for a given board state until all possibilities are exhausted or a goal state is found.
     * @param pegboard The pegboard object to have its successors generated
     */
    protected abstract void generateSuccessors(Pegboard pegboard);


    /**
     * Generates the possible moves that can be made from a given board.
     *
     * @param pegboard The state to have its next possible moves made
     * @return A list of possible moves
     */
    public ArrayList<Pegboard> getNextMoves(Pegboard pegboard) {

        ArrayList<Pegboard> possibleMoves = new ArrayList<>();
        int[][] board = pegboard.getBoard();

        for (int i = 0; i < board.length; i++) {

            for (int j = 0; j < board.length; j++) {

                // If on an empty space, we might be able to make a move
                if (board[i][j] == 0) {

                    // Scenario:  1      0
                    //            1  =>  0
                    //            0      1
                    if ((i - 2) >= 0 && board[i-1][j] == 1 && board[i-2][j] == 1) {

                        Pegboard copy = new Pegboard(pegboard);

                        copy.setBoard(i, j, 1);
                        copy.setBoard(i-1, j, 0);
                        copy.setBoard(i-2, j, 0);

                        possibleMoves.add(copy);
                    }

                    // Scenario:  0      1
                    //            1  =>  0
                    //            1      0
                    if ((i + 2) < board.length && board[i+1][j] == 1 && board[i+2][j] == 1) {

                        Pegboard copy = new Pegboard(pegboard);

                        copy.setBoard(i, j, 1);
                        copy.setBoard(i+1, j, 0);
                        copy.setBoard(i+2, j, 0);

                        possibleMoves.add(copy);
                    }

                    // Scenario:  1   1   0   =>   0   0   1
                    if ((j - 2) >= 0 && board[i][j-1] == 1 && board[i][j-2] == 1) {

                        Pegboard copy = new Pegboard(pegboard);

                        copy.setBoard(i, j, 1);
                        copy.setBoard(i, j-1, 0);
                        copy.setBoard(i, j-2, 0);

                        possibleMoves.add(copy);
                    }

                    // Scenario:  0   1   1   =>   1   0   0
                    if ((j + 2) < board.length && board[i][j+1] == 1 && board[i][j+2] == 1) {

                        Pegboard copy = new Pegboard(pegboard);

                        copy.setBoard(i, j, 1);
                        copy.setBoard(i, j+1, 0);
                        copy.setBoard(i, j+2, 0);

                        possibleMoves.add(copy);
                    }
                }
            }
        }

        return possibleMoves;
    }


    /**
     * Checks whether a pegboard has been completed or not.
     * If there is 1 peg left, then the board is completed.
     *
     * @param board The pegboard state to check
     * @return Whether the pegboard has 1 peg left
     */
    protected boolean isCompleted(int[][] board) {

        int numPegs = 0;

        for (int[] row : board) {

           for (int value : row) {

               if (value == 1) {

                   numPegs++;
               }
           }
        }

        return numPegs == 1;
    }


    /**
     * Outputs the results of the search algorithm, including time and states visited.
     *
     * @param startTime The start time of the algorithm
     * @param statesExplored The number of states explored
     * @param foundGoal Whether a goal state was reached
     */
    protected void printResults(long startTime, int statesExplored, boolean foundGoal) {

        String messageHeader = foundGoal ? "-- Goal State Found --" : "-- No Goal State Found --";

        System.out.println("\n" + messageHeader);
        System.out.println("Unique states explored: " + statesExplored);
        System.out.println("Search time: " + (System.currentTimeMillis() - startTime) * 0.001 + "s");

        System.exit(0);
    }
}
