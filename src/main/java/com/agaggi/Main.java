package com.agaggi;

import static java.lang.Integer.parseInt;

import com.agaggi.algorithms.*;

public class Main {

    public static void main(String[] args) {

        try {

            String searchAlgorithm = args[0].toLowerCase();
            int size = parseInt(args[1]);

            if (size < 4 || size > 8) {

                System.out.println("Invalid dimension entered. See README for details.");
                System.exit(1);
            }

            switch (searchAlgorithm) {

                case "breadth":
                    Breadth breadth = new Breadth(size);
                    breadth.run();
                    break;

                case "depth":
                    Depth depth = new Depth(size);
                    depth.run();
                    break;

                case "greedy":
                    Greedy greedy = new Greedy(size);
                    greedy.run();
                    break;

                case "a-star":
                    A_Star a_star = new A_Star(size);
                    a_star.run();
                    break;

                default:
                    System.out.println("Invalid search algorithm entered. See README for details.");
            }
        }

        catch (ArrayIndexOutOfBoundsException e) {

            System.out.println("Not enough arguments entered. See README for details.");
        }
    }
}
